import { AfterContentChecked, AfterContentInit, Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit {
  @Input('srvElement') element: {type: string, name: string, content: string};

  constructor() {
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   console.log('ngOnChanes called!')
  //   console.log(changes.element.previousValue)
  // }

  ngOnInit(): void {
  }

  // ngDoCheck(): void {
  //   console.log('ngDoCheck called')
  // }

  // ngAfterContentInit(): void {
  //   console.log('ngAfterContentInit called')
  // }

  // ngAfterContentChecked(): void {
  //   console.log('Aftercontent checked called')
  // }
}
